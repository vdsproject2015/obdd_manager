# README #

This README documents the necessary steps to get our project up and running.

### What is this repository for? ###

* This repository for implementing a minimal BDD package in C++ and verifying it by unit tests 

### How do I get set up? ###

* First the repository should be cloned in a local directory by this command:  
   $ git clone https://user_name@bitbucket.org/vdsproject2015/obdd_manager.git

* In order to build a static library from the source code, need to go to that local directory and use the command:  
   $ scons 

* After this compilation in order to run the unittest use the command:  
   $ scons -test
 
### Repo owners are vtan_nguyen and selin_tozman ###