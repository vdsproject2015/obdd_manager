//! Implementation of manager packet for Reduced Ordered Binary Decision Diagram ROBDD
/*!
  Authors {Viet Tan Nguyen,Selin Tozman}
*/
#include "OBDD.hpp"

ID manager::createVar(const string& name)
{
  //-------Insert new "empty" node to the table
  BDD_table.push_back(OBDD());

  //-------Initialize value for the new node
  BDD_table[int(BDD_table.size())-1].name = name;
  BDD_table[int(BDD_table.size())-1].ID_topVar = BDD_table.size()-1;
  BDD_table[int(BDD_table.size())-1].ID_sink_1 = 1;
  BDD_table[int(BDD_table.size())-1].ID_sink_0 = 0;

  //-------Create new key for computed table
  OBDD2ID.emplace(BDD_table[int(BDD_table.size())-1],BDD_table.size()-1);

  return BDD_table.size()-1;
};

manager::manager()
{
  // Allocate first 2 elements 0 and 1 for the table
  // BDD_table = new OBDD[2];
  // size = 2;
  BDD_table.push_back(OBDD());
  BDD_table.push_back(OBDD());

  // Assign the first element of the table "false"
  BDD_table[0].name = "false";
  BDD_table[0].ID_topVar = 0;
  OBDD2ID.emplace(BDD_table[0],0);

  // Assign the second element of the table "true"
  BDD_table[1].name = "true";
  BDD_table[1].ID_topVar = 1;
  OBDD2ID.emplace(BDD_table[1],1);
}

const ID manager::True()
{
  return BDD_table[1].ID_topVar;
};

const ID manager::False()
{
  return BDD_table[0].ID_topVar;
};

bool manager::isConstant(const ID f)
{
  if(f >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
  {
    if (f == (unsigned int)0 || f ==(unsigned int)1)
      return true;
    else
      return false;
  }
};

bool manager::isVariable(const ID f)
{
  if(f >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
  {
    if (BDD_table[f].ID_sink_0 == (ID)0 && BDD_table[f].ID_sink_1 == (ID)1)
      return true;
    else
      return false;
  }
};

size_t manager::topvar(const ID f)
{
  if(f >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
    return BDD_table[f].ID_topVar;
}

ID manager::coFactorTrue(const ID f)
{
  if(f >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
    return BDD_table[f].ID_sink_1;
}

ID manager::coFactorFalse(const ID f)
{
  if(f >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
    return BDD_table[f].ID_sink_0;
}

void manager::findVars(const ID& root, set<ID>& vars_of_root)
{
  if(root >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
  {
    // ** Check the corner cases:
    // If a node is constant, do nothing else insert its top variable ID
    if(isConstant(root) == false)
    {
      // Insert the ID of variable in the set if it's not in the set
      if(vars_of_root.find(BDD_table[root].ID_topVar) == vars_of_root.end())
          vars_of_root.insert(BDD_table[root].ID_topVar);

    // ** Do the function recursively with the nodes in true and false branch
    findVars(BDD_table[root].ID_sink_1,vars_of_root);
    findVars(BDD_table[root].ID_sink_0,vars_of_root);
    }
  }
}

void manager::findNodes(const ID& root, set<ID>& nodes_of_root)
{
  if(root >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
  {
    // ** Check the corner cases:
    // If a node is not constant, insert its ID and apply recursive function to
    // its successors
    if(isConstant(root) == false)
    {
      // Insert the ID of node which is not in the set
      if(nodes_of_root.find(root) == nodes_of_root.end())
          nodes_of_root.insert(root);

      // ** Do the function recursively with the nodes in true and false branch
      findNodes(BDD_table[root].ID_sink_1,nodes_of_root);
      findNodes(BDD_table[root].ID_sink_0,nodes_of_root);
    }
    // If a node is constant, insert its ID
    else
      if(nodes_of_root.find(root) == nodes_of_root.end())
          nodes_of_root.insert(root);
  }
}

string manager::getTopVarName(const ID& root)
{
  if(root >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
    return BDD_table[BDD_table[root].ID_topVar].name;
}

ID manager::ite(const ID i, const ID t, const ID e)
{
  if(i >= BDD_table.size() || t >= BDD_table.size() || e >= BDD_table.size())
    // The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
  {
    // ** Check the corner cases:
    // Check if i is constant, then return t or e
    if(i == (ID)1)
      return t;
    else if(i == (ID)0)
      return e;
    // Check if t and e are the same, return t
    else if(t == e)
      return t;
    else
    // Case top variable of i is a variable for sure
    {
      ID top_var;
      ID ID_T,ID_F;
      set<ID> setVar;

      // ** Compare top variable of 3 nodes, chose the one with lowest ID (highest order)
      // ** Top variable has ID greater than 1
      // Case only top variable of t smaller than or equal to 1 (t is a constant)
      if(BDD_table[t].ID_topVar <= (ID)1)
        if(BDD_table[e].ID_topVar <= (ID)1)
          top_var = BDD_table[i].ID_topVar;
        else
          top_var = std::min(BDD_table[i].ID_topVar,BDD_table[e].ID_topVar);
      // Case only top variable of e smaller than or equal to 1 (e is a constant)
      else if(BDD_table[e].ID_topVar <= (ID)1)
        top_var = std::min(BDD_table[i].ID_topVar,BDD_table[t].ID_topVar);
      // Case top variable of i,t,e are not constant
      else
        top_var = std::min(std::min(BDD_table[i].ID_topVar,BDD_table[e].ID_topVar),BDD_table[t].ID_topVar);

      // ** Recursively find the ID of ite operation for 2 successor nodes
      ID_T = ite(coFactorTrue(i,top_var),coFactorTrue(t,top_var),coFactorTrue(e,top_var));
      ID_F = ite(coFactorFalse(i,top_var),coFactorFalse(t,top_var),coFactorFalse(e,top_var));

      // ** Check if there is already a node in unique table
      OBDD OBDDTemp;
      OBDDTemp.ID_topVar=top_var;
      OBDDTemp.ID_sink_1=ID_T;
      OBDDTemp.ID_sink_0=ID_F;

      if(OBDD2ID.find(OBDDTemp) != OBDD2ID.end())
        return OBDD2ID[OBDDTemp];
      else
      {
        // ** Insert new node to the table when it's not existed
        BDD_table.push_back(OBDDTemp);

        //-------Create new key for computed table
        OBDD2ID.emplace(OBDDTemp,BDD_table.size()-1);

        return BDD_table.size()-1;
      }
    }
  }
}

ID manager::coFactorTrue(const ID f, const ID x)
{
  if(f >= BDD_table.size() || x >= BDD_table.size())
    // ** The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");

    // ** Check x is variable or not
  if(isVariable(x) == false)
    throw BDDidInvalidException("Error: The second operand is't a variable");

  ID ID_T,ID_F;

  // ** Check the corner cases:
  //-------The order of x is higher than that of top variable of f, return f
  if (x < BDD_table[f].ID_topVar)
    return f;
  //-------f is constant, return f
  if (isConstant(f))
    return f;
  // if top variable of f is x, return true successor node
  if(BDD_table[f].ID_topVar == x)
        return BDD_table[f].ID_sink_1;

  // Recursively find the cofactor True for both successor nodes
    ID_T = coFactorTrue(BDD_table[f].ID_sink_1,x);
    ID_F = coFactorTrue(BDD_table[f].ID_sink_0,x);

  //-------create new node if it doesn't exist in table or return the existed node
    return ite(BDD_table[f].ID_topVar,ID_T,ID_F);
}

ID manager::coFactorFalse(const ID f, const ID x)
{
  if(f >= BDD_table.size() || x >= BDD_table.size())
    // ** The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");

    // ** Check x is variable or not
  if(isVariable(x) == false)
    throw BDDidInvalidException("Error: The second operand is't a variable");

  ID ID_T,ID_F;

  // ** Check the corner cases:
  //-------The order of x is higher than that of top variable of f, return f
  if (x < BDD_table[f].ID_topVar)
    return f;
  //-------f is constant, return f
  if (isConstant(f))
    return f;
  // if top variable of f is x, return true successor node
  if(BDD_table[f].ID_topVar == x)
        return BDD_table[f].ID_sink_0;

  // Recursively find the cofactor True for both successor nodes
    ID_T = coFactorFalse(BDD_table[f].ID_sink_1,x);
    ID_F = coFactorFalse(BDD_table[f].ID_sink_0,x);

  //-------create new node if it doesn't exist in table or return the existed node
    return ite(BDD_table[f].ID_topVar,ID_T,ID_F);
}

ID manager::and2(const ID a, const ID b)
{
  if(a >= BDD_table.size() || b >= BDD_table.size())
    // ** The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
    // Implement the function using if then else method
    ite (a,b,0);
}

ID manager::or2(const ID a, const ID b)
{
  if(a >= BDD_table.size() || b >= BDD_table.size())
    // ** The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
     // Implement the function using if then else method
    ite (a,1,b);
}


ID manager::xor2(const ID a, const ID b)
{
  if(a >= BDD_table.size() || b >= BDD_table.size())
    // ** The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
     // Implement the function using if then else method
    ite (a,ite(b,0,1),b);
}


ID manager::neq(const ID a)
{
  if(a >= BDD_table.size())
    // ** The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
     // Implement the function using if then else method
    ite (a, 0, 1);
}

ID manager::nand2(const ID a,const ID b)
{
  if(a >= BDD_table.size() || b >= BDD_table.size())
    // ** The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
     // Implement the function using if then else method
    ite ( ite (a,b,0), 0, 1);
}

ID manager::nor2(const ID a,const ID b)
{
  if(a >= BDD_table.size() || b >= BDD_table.size())
    // ** The ID is not in the table
    throw BDDidInvalidException("BDD ID is not in the table");
  else
     // Implement the function using if then else method
    ite ( ite (a,1,b), 0, 1);
}


