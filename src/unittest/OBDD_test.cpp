/**
 *
 * This class contains the implementation of Unit Tests used to test the manager class.
 *
 * \author Viet Tan Nguyen
 * \author Selin Tozman
 *
 */
#include "OBDD_test.hpp"

//CppUnit: register suite into the TestFactoryRegistry
CPPUNIT_TEST_SUITE_REGISTRATION(OBDD_test);

 OBDD_test::OBDD_test()
 {
  /**< empty constuctor.*/
 }

//CppUnit: override setUp() to initialize the variables
 //! initialize objects according to the example given in the class
  /*!
     Create a TestTable with 10 nodes, define each node with
     its name, top variable and high and low cofactor nodes
  */
void OBDD_test::setUp(void) {

  /**< Create a ROBDD table in class manager m2 with size of 11 by hand */

  //! node "2"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[2].name = "a";
  m2.BDD_table[2].ID_topVar = 2;
  m2.BDD_table[2].ID_sink_1 = 1;
  m2.BDD_table[2].ID_sink_0 = 0;
  m2.OBDD2ID.emplace(m2.BDD_table[2],2);

  //! node "3"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[3].name = "b";
  m2.BDD_table[3].ID_topVar = 3;
  m2.BDD_table[3].ID_sink_1 = 1;
  m2.BDD_table[3].ID_sink_0 = 0;
  m2.OBDD2ID.emplace(m2.BDD_table[3],3);

  //! node "4"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[4].name = "c";
  m2.BDD_table[4].ID_topVar = 4;
  m2.BDD_table[4].ID_sink_1 = 1;
  m2.BDD_table[4].ID_sink_0 = 0;
  m2.OBDD2ID.emplace(m2.BDD_table[4],4);

  //! node "5"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[5].name = "d";
  m2.BDD_table[5].ID_topVar = 5;
  m2.BDD_table[5].ID_sink_1 = 1;
  m2.BDD_table[5].ID_sink_0 = 0;
  m2.OBDD2ID.emplace(m2.BDD_table[5],5);

  //! node "6"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[6].name = "f1 = or(2,3)";
  m2.BDD_table[6].ID_topVar = 2;
  m2.BDD_table[6].ID_sink_1 = 1;
  m2.BDD_table[6].ID_sink_0 = 3;
  m2.OBDD2ID.emplace(m2.BDD_table[6],6);

  //! node "7"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[7].name = "f2 = and(5,4)";
  m2.BDD_table[7].ID_topVar = 4;
  m2.BDD_table[7].ID_sink_1 = 5;
  m2.BDD_table[7].ID_sink_0 = 0;
  m2.OBDD2ID.emplace(m2.BDD_table[7],7);

  //! node "8"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[8].ID_topVar = 3;
  m2.BDD_table[8].ID_sink_1 = 7;
  m2.BDD_table[8].ID_sink_0 = 0;
  m2.OBDD2ID.emplace(m2.BDD_table[8],8);

  //! node "9"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[9].name = "f3 = f1 and f2";
  m2.BDD_table[9].ID_topVar = 2;
  m2.BDD_table[9].ID_sink_1 = 7;
  m2.BDD_table[9].ID_sink_0 = 8;
  m2.OBDD2ID.emplace(m2.BDD_table[9],9);

  //! node "10"
  m2.BDD_table.push_back(OBDD());
  m2.BDD_table[10].name = "f4 = f3 and a";
  m2.BDD_table[10].ID_topVar = 2;
  m2.BDD_table[10].ID_sink_1 = 4;
  m2.BDD_table[10].ID_sink_0 = 0;
  m2.OBDD2ID.emplace(m2.BDD_table[10],10);
}

// CppUnit: override tearDown() to release any permanent resources
// you allocated in setUp()

void OBDD_test::tearDown(void) {

}

/// Method for Unit Test: checks the constructor.
//Creates two objects and Checks the ID, topVar and name of them

void OBDD_test::constructorTest(void){

  CPPUNIT_ASSERT_EQUAL((size_t)0, m1.BDD_table[0].ID_topVar);
  CPPUNIT_ASSERT_EQUAL( (string) "false", m1.BDD_table[0].name);
  CPPUNIT_ASSERT_EQUAL((size_t)1, m1.BDD_table[1].ID_topVar);
  CPPUNIT_ASSERT_EQUAL((string)"true", m1.BDD_table[1].name);
  CPPUNIT_ASSERT_EQUAL((size_t)2, m1.BDD_table.size());
}


/// Method for Unit Test: checks the createVar().
///Checks the ID, topVar, name, ID of high cofactor and low cofactor.
void OBDD_test::createVarTest(void) {
  int i ;
  ID ID1,ID2 ;
  ID1 = m1.createVar("abc"); /**< create two variables*/
  ID2 = m1.createVar("a");
  CPPUNIT_ASSERT_EQUAL((ID)3,ID2); /**< check if the ID2 is 3*/
  CPPUNIT_ASSERT_EQUAL((ID)2,ID1); /**< check if the ID1 is 2*/
  for(i=0;i<m1.BDD_table.size();i++)
  {
    if(m1.BDD_table[i].ID_topVar == ID1)  /**< if the variable is ID1 */
    {
      CPPUNIT_ASSERT_EQUAL((ID)2,m1.BDD_table[i].ID_topVar);  /**< check the top variable of the ID1 */
      CPPUNIT_ASSERT_EQUAL((string)"abc",m1.BDD_table[i].name); /**< check the name of variable ID1 */
      CPPUNIT_ASSERT_EQUAL((ID)1,m1.BDD_table[i].ID_sink_1); /**< check if  the high cofactor of ID2 is 1 */
      CPPUNIT_ASSERT_EQUAL((ID)0,m1.BDD_table[i].ID_sink_0); /**< check if the low cofactor of ID1 is 0 */
    }
    if(m1.BDD_table[i].ID_topVar == ID2)   /**< check all steps in a same way as ID1 */
    {
      CPPUNIT_ASSERT_EQUAL((ID)3,m1.BDD_table[i].ID_topVar);
      CPPUNIT_ASSERT_EQUAL((string)"a",m1.BDD_table[i].name);
      CPPUNIT_ASSERT_EQUAL((ID)1,m1.BDD_table[i].ID_sink_1);
      CPPUNIT_ASSERT_EQUAL((ID)0,m1.BDD_table[i].ID_sink_0);
    }

  }

}
/// Method for Unit Test: checks the True().
///Checks if the constant true of the Unique table is ID1
void OBDD_test::trueTest(void) {
  CPPUNIT_ASSERT_EQUAL((ID)1,m1.True());
}

/// Method for Unit Test: checks the False().
///Checks if the constant false of the Unique table is ID0
void OBDD_test::falseTest(void) {
  CPPUNIT_ASSERT_EQUAL((ID)0,m1.False());
}

/// Method for Unit Test: checks isCostant().
void OBDD_test::isConstantTest(void) {
  /// check by creating variable "a".
  ID ID1 = m1.createVar("a");
  CPPUNIT_ASSERT_EQUAL(true, m1.isConstant(1));
  CPPUNIT_ASSERT_EQUAL(true, m1.isConstant(0));
  ///check if the variable's cofactors are constant.
  CPPUNIT_ASSERT_EQUAL(true, m1.isConstant(m1.BDD_table[1].ID_sink_1));
  CPPUNIT_ASSERT_EQUAL(true, m1.isConstant(m1.BDD_table[1].ID_sink_0));
  CPPUNIT_ASSERT_EQUAL(false, m1.isConstant(ID1));
  ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m1.isConstant((ID)5),BDDidInvalidException);

  ///check by TestTable.
  CPPUNIT_ASSERT_EQUAL(true, m2.isConstant(1)); /**< It is constant */
  CPPUNIT_ASSERT_EQUAL(false, m2.isConstant(7)); /**< It is function */
  CPPUNIT_ASSERT_EQUAL(false, m2.isConstant(7)); /**< It is variable */
  ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m2.isConstant((ID)20),BDDidInvalidException);

}

/// Method for Unit Test: checks isVariable().
void OBDD_test::isVariableTest(void) {
  ///check by creating variable "a".
  ID ID1 = m1.createVar("a");
  CPPUNIT_ASSERT_EQUAL(false, m1.isVariable(1)); /**< It is a constant */
  CPPUNIT_ASSERT_EQUAL(false, m1.isVariable(0)); /**< It is a constant */
  CPPUNIT_ASSERT_EQUAL(true, m1.isVariable(2)); /**< It is a variable */
  ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m1.isVariable((ID)4),BDDidInvalidException);

  ///check for the whole TestTable.
  CPPUNIT_ASSERT_EQUAL(false, m2.isVariable(0)); /**< It is constant */
  CPPUNIT_ASSERT_EQUAL(false, m2.isVariable(1)); /**< It is constant */

   for(int i=2; i<m2.BDD_table.size() ;i++)  /**< Check for the nodes starting from the third node */
  { /// if it is a variable.
    if (( m2.isConstant(m2.BDD_table[i].ID_sink_1) == true) && ( m2.isConstant(m2.BDD_table[i].ID_sink_0) == true ))
    {
     CPPUNIT_ASSERT_EQUAL(true, m2.isVariable(i));
    }
    else
     {
     ///If it is not a variable.
     CPPUNIT_ASSERT_EQUAL(false, m2.isVariable(i));
     }
  }
  ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m2.isVariable((ID)23),BDDidInvalidException);

}

/// Method for Unit Test: checks topVar().
void OBDD_test::topvarTest(void) {
  /// check by creating variables "a" and "abcd".
  ID ID1 = m1.createVar("a");
  ID ID2 = m1.createVar("abcd");
  ///check for the variables.
  CPPUNIT_ASSERT_EQUAL(m1.topvar(ID1),m1.BDD_table[ID1].ID_topVar);
  CPPUNIT_ASSERT_EQUAL(m1.topvar(ID2),m1.BDD_table[ID2].ID_topVar);
  ///check for the constants.
  CPPUNIT_ASSERT_EQUAL(m1.topvar(0),m1.BDD_table[0].ID_topVar);
  CPPUNIT_ASSERT_EQUAL(m1.topvar(1),m1.BDD_table[1].ID_topVar);
  ///check for the high cofactor of the variable.
  CPPUNIT_ASSERT_EQUAL(m1.topvar(1),m1.BDD_table[m1.BDD_table[2].ID_sink_1].ID_topVar);
  ///check for the low cofactor of the variable.
  CPPUNIT_ASSERT_EQUAL(m1.topvar(0),m1.BDD_table[m1.BDD_table[2].ID_sink_0].ID_topVar);
   ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m1.topvar((ID)6),BDDidInvalidException);

  ///check for the whole TestTable
   for(int i=0; i<m2.BDD_table.size() ;i++)
  {
     CPPUNIT_ASSERT_EQUAL(m2.topvar(ID1),m2.BDD_table[ID1].ID_topVar);
  }

  ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m2.topvar((ID)23),BDDidInvalidException);
}

/// Method for Unit Test: checks coFactorTrue().
void OBDD_test::coFactorTrueTest(void) {
  /// check by creating variables "a" and "abcd".
  ID ID1 = m1.createVar("a");
  ID ID2 = m1.createVar("abcd");
  ///checks for the variable a.
  CPPUNIT_ASSERT_EQUAL(m1.coFactorTrue(ID1),m1.BDD_table[ID1].ID_sink_1);
  ///checks for the variable abcd.
  CPPUNIT_ASSERT_EQUAL(m1.coFactorTrue(ID2),m1.BDD_table[ID1].ID_sink_1);
  ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m1.coFactorTrue((ID)4),BDDidInvalidException);

   ///check for the whole variables in the TestTable.
    for(int i=0; i<m2.BDD_table.size() ;i++)
  {
      CPPUNIT_ASSERT_EQUAL(m2.coFactorTrue(i),m2.BDD_table[i].ID_sink_1);
  }
   ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m2.coFactorTrue((ID)23),BDDidInvalidException);

}


/// Method for Unit Test: checks coFactorFalse().
void OBDD_test::coFactorFalseTest(void) {
  /// check by creating variables "a" and "abcd".
  ID ID1 = m1.createVar("a");
  ID ID2 = m1.createVar("abcd");
  ///checks for the variable a.
  CPPUNIT_ASSERT_EQUAL(m1.coFactorFalse(ID1),m1.BDD_table[ID1].ID_sink_0);
  ///checks for the variable abcd.
  CPPUNIT_ASSERT_EQUAL(m1.coFactorFalse(ID2),m1.BDD_table[ID1].ID_sink_0);
  ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m1.coFactorFalse((ID)4),BDDidInvalidException);

  ///check for the whole  TestTable.
    for(int i=0; i<m2.BDD_table.size() ;i++)
  {
     CPPUNIT_ASSERT_EQUAL(m2.coFactorFalse(i),m2.BDD_table[i].ID_sink_0);
  }
   ///check when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m2.coFactorFalse((ID)23),BDDidInvalidException);
}

/// Method for Unit Test: checks getTopVarName ()
void OBDD_test::getTopVarNameTest(void) {
  /// check by creating variables "a" and "abcd".
  ID ID1 = m1.createVar("a");
  ID ID2 = m1.createVar("abcd");
   ///check for variables.
  CPPUNIT_ASSERT_EQUAL((string)"a",m1.getTopVarName(2));
  CPPUNIT_ASSERT_EQUAL((string)"abcd",m1.getTopVarName(3));
  ///check for constants.
  CPPUNIT_ASSERT_EQUAL((string)"true",m1.getTopVarName(1));
  CPPUNIT_ASSERT_EQUAL((string)"false",m1.getTopVarName(0));
  ///when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m1.getTopVarName(4), BDDidInvalidException);

  ///check for the TestTable.
  CPPUNIT_ASSERT_EQUAL((string)"true",m2.getTopVarName(1)); /**<check for constants*/
  CPPUNIT_ASSERT_EQUAL((string)"false",m2.getTopVarName(0));
  ///check for variable
  CPPUNIT_ASSERT_EQUAL((string)"a",m2.getTopVarName(2));
  ///check for functions
  CPPUNIT_ASSERT_EQUAL((string)"b",m2.getTopVarName(8));
  ///when the ID is not belong to the table.
  CPPUNIT_ASSERT_THROW(m2.getTopVarName(25),BDDidInvalidException);
}

/// Method for Unit Test: checks findVars()
void OBDD_test::findVarsTest(void) {

  ///Create 2 empty ID sets to compare
  set<ID> s1,s ;

  /// test node "true"
  s1.insert(1);
  m2.findVars(1,s);
  CPPUNIT_ASSERT_EQUAL(false , s1 == s);
  s1.clear(); /**<clear in order to reuse*/
  s.clear();  /**<clear in order to reuse*/

  /// test node "false"
  s1.insert(0);
  m2.findVars(0,s);
  CPPUNIT_ASSERT_EQUAL(false , s1 == s);
  s1.clear(); /**<clear in order to reuse*/
  s.clear(); /**<clear in order to reuse*/

  ///test variable "a" as a root
  s1.insert(2);
  m2.findVars(2,s);
  CPPUNIT_ASSERT_EQUAL(true , s1 == s);
  s1.clear(); /**<clear in order to reuse*/
  s.clear();  /**<clear in order to reuse*/


   ///test the function with ID6 as a root
  s1.insert(2);
  s1.insert(3);
  m2.findVars(6,s);
  CPPUNIT_ASSERT_EQUAL(true , s1 == s);
  s1.clear(); /**<clear in order to reuse*/
  s.clear();  /**<clear in order to reuse*/


  /// test the function with ID9 as a root
  s1.insert(2);
  s1.insert(3);
  s1.insert(4);
  s1.insert(5);
  m2.findVars(9,s);
  CPPUNIT_ASSERT_EQUAL(true , s1 == s);
  ///when the ID is not belong to the table
  CPPUNIT_ASSERT_THROW(m2.findVars(25,s),BDDidInvalidException);

}

/// Method for Unit Test: checks findNodes()
void OBDD_test::findNodesTest(void) {
  ///Create 2 empty ID sets to compare
  set<ID> s1,s;

  /// test node "true"
  s1.insert(1);
  m2.findNodes(1,s);
  CPPUNIT_ASSERT_EQUAL(true , s1 == s);
  s1.clear(); /**<clear in order to reuse*/
  s.clear();  /**<clear in order to reuse*/

  /// test node "false"
  s1.insert(0);
  m2.findNodes(0,s);
  CPPUNIT_ASSERT_EQUAL(true , s1 == s);
  s1.clear(); /**<clear in order to reuse*/
  s.clear();  /**<clear in order to reuse*/

 ///test variable "a" as a root
  s1.insert(0);
  s1.insert(1);
  s1.insert(2);
  m2.findNodes(2,s);
  CPPUNIT_ASSERT_EQUAL(true , s1 == s);
  s1.clear(); /**<clear in order to reuse*/
  s.clear();  /**<clear in order to reuse*/

  /// test the function with ID6 as a root
  s1.insert(0);
  s1.insert(1);
  s1.insert(3);
  s1.insert(6);
  m2.findNodes(6,s);
  CPPUNIT_ASSERT_EQUAL(true , s1 == s);
  s1.clear(); /**<clear in order to reuse*/
  s.clear();  /**<clear in order to reuse*/


  /// test the function with ID9 as a root
  s1.insert(9);
  s1.insert(7);
  s1.insert(8);
  s1.insert(5);
  s1.insert(1);
  s1.insert(0);
  m2.findNodes(9,s);
  CPPUNIT_ASSERT_EQUAL(true , s1 == s);

  ///when the ID is not belong to the table
  CPPUNIT_ASSERT_THROW(m2.findNodes(25,s),BDDidInvalidException);
}

/// Method for Unit Test: checks coFactorTrue2()
void OBDD_test::coFactorTrue2Test(void) {
  ///when function is not belong to the table
  CPPUNIT_ASSERT_THROW(m2.coFactorTrue(25,2),BDDidInvalidException);
  ///when variable is not belong to the table
  CPPUNIT_ASSERT_THROW(m2.coFactorTrue(2,25),BDDidInvalidException);
  ///when the function called by a constant
  CPPUNIT_ASSERT_THROW(m2.coFactorTrue(8,1),BDDidInvalidException);
  ///When the variable in not in the function
  CPPUNIT_ASSERT_EQUAL((ID)6,m2.coFactorTrue(6,5));
  ///When the order of variable is higher than that of top variable of root node
  CPPUNIT_ASSERT_EQUAL((ID)7,m2.coFactorTrue(7,2));

  ///If the function is actually a variable
   for(int i=0; i<m2.BDD_table.size() ;i++)
  {
    if (m2.isVariable(i) == true ) /**< if it is a variable */
    {
     CPPUNIT_ASSERT_EQUAL((ID)1,m2.coFactorTrue(i,i));
    }
  }
  ///When the function called by a variable
  CPPUNIT_ASSERT_EQUAL((ID)1,m2.coFactorTrue(6,2)); /**< two recursion */
  CPPUNIT_ASSERT_EQUAL((ID)7,m2.coFactorTrue(9,2)); /**< three recursion*/
  CPPUNIT_ASSERT_EQUAL((ID)12,m2.coFactorTrue(9,5)); /**< four recursion*/
}

/// Method for Unit Test: checks coFactorFalse2()
void OBDD_test::coFactorFalse2Test(void) {
  ///when function is not belong to the table
  CPPUNIT_ASSERT_THROW(m2.coFactorFalse(25,2),BDDidInvalidException);
  ///when variable is not belong to the table
  CPPUNIT_ASSERT_THROW(m2.coFactorFalse(2,25),BDDidInvalidException);
  ///when the function called by a constant
  CPPUNIT_ASSERT_THROW(m2.coFactorFalse(6,1),BDDidInvalidException);
  ///When the variable in not in the function
  CPPUNIT_ASSERT_EQUAL((ID)6,m2.coFactorFalse(6,5));
  ///When the order of variable is higher than that of top variable of root node
  CPPUNIT_ASSERT_EQUAL((ID)7,m2.coFactorFalse(7,2));

  ///If the function is actually a variable
   for(int i=0; i<m2.BDD_table.size() ;i++)
  {
    if (m2.isVariable(i) == true ) /**< if it is a variable */
    {
     CPPUNIT_ASSERT_EQUAL((ID)0,m2.coFactorFalse(i,i));
    }
  }
  ///When the function called by a variable
  CPPUNIT_ASSERT_EQUAL((ID)0,m2.coFactorFalse(10,2)); /**< one recursion */
  CPPUNIT_ASSERT_EQUAL((ID)8,m2.coFactorFalse(9,2));  /**< two recursion */
}

/// Method for Unit Test: checks ite()
 void OBDD_test::iteTest(void) {

  ///If ID of the nodes are not in the table, go exception
  CPPUNIT_ASSERT_THROW(m2.ite(23,2,3),BDDidInvalidException);
  CPPUNIT_ASSERT_THROW(m2.ite(2,3,23),BDDidInvalidException);
  CPPUNIT_ASSERT_THROW(m2.ite(23,3,2),BDDidInvalidException);

  ///If i=1, return second term
  CPPUNIT_ASSERT_EQUAL((ID)3,m2.ite(1,3,4));
  /// If i=0, return third term
  CPPUNIT_ASSERT_EQUAL((ID)4,m2.ite(0,3,4));
  /// If t=1 and e=1, return 1
  CPPUNIT_ASSERT_EQUAL((ID)1,m2.ite(3,1,1));
  /// If t=0 and e=0, return 0
  CPPUNIT_ASSERT_EQUAL((ID)0,m2.ite(5,0,0));

  ///check for the TestTable
  CPPUNIT_ASSERT_EQUAL((ID)6,m2.ite(2,1,3));  /**<when t is not a variable */
  CPPUNIT_ASSERT_EQUAL((ID)7,m2.ite(4,5,0));  /**<when e is not a variable */
  CPPUNIT_ASSERT_EQUAL((ID)8,m2.ite(3,7,0));
  CPPUNIT_ASSERT_EQUAL((ID)9,m2.ite(7,6,0));

  ///check by creating a table for the function (a+b).(c.d).
  ///create four variables
   m1.createVar("a");
   m1.createVar("b");
   m1.createVar("c");
   m1.createVar("d");

   CPPUNIT_ASSERT_EQUAL((ID)6,m1.ite(2,1,3));/**< a+b */
   CPPUNIT_ASSERT_EQUAL((ID)7,m1.ite(4,5,0));/**< c.d */
   CPPUNIT_ASSERT_EQUAL((ID)8,m1.ite(3,7,0));
   CPPUNIT_ASSERT_EQUAL((ID)9,m1.ite(6,7,0));/**< (a+b).(c.d) */
}

/// Method for Unit Test: checks and2()
  void OBDD_test::and2Test(void)
  {
  ///when node is not belong to the table
    CPPUNIT_ASSERT_THROW(m2.and2(25,2),BDDidInvalidException);
    CPPUNIT_ASSERT_THROW(m2.and2(2,25),BDDidInvalidException);

    ///check by TestTable for the operation and(4,5)
    ID f1 = m2.and2(4,5); /**<create function f1 */
    CPPUNIT_ASSERT_EQUAL((ID)7,f1); /**< check the ID of the function is 7 */
    CPPUNIT_ASSERT_EQUAL((ID)4,m2.BDD_table[f1].ID_topVar);  /**< check the top variable is 4 */
    CPPUNIT_ASSERT_EQUAL((ID)5,m2.BDD_table[f1].ID_sink_1);/**< check if  the high cofactor is 5 */
    CPPUNIT_ASSERT_EQUAL((ID)0,m2.BDD_table[f1].ID_sink_0); /**< check if the low cofactor is 0 */
    ///check by TestTable for the operation and(6,7)
    CPPUNIT_ASSERT_EQUAL((ID)9,m2.and2(6,7)); /**< check the ID of the function is 9 */
    CPPUNIT_ASSERT_EQUAL((ID)2,m2.BDD_table[m2.and2(6,7)].ID_topVar);  /**< check the top variable is 2 */
    CPPUNIT_ASSERT_EQUAL((ID)7,m2.BDD_table[m2.and2(6,7)].ID_sink_1); /**< check if  the high cofactor is 7 */
    CPPUNIT_ASSERT_EQUAL((ID)8,m2.BDD_table[m2.and2(6,7)].ID_sink_0); /**< check if the low cofactor is 8 */

   ///check by creating a table for the function f= (a.b)
   m1.createVar("a");/**< create variables */
   m1.createVar("b");
   ID f2 = m1.and2(2,3); /**< create function f */

   CPPUNIT_ASSERT_EQUAL((ID)4,f2); /**< check the ID of the function is 4 */
   CPPUNIT_ASSERT_EQUAL((ID)2,m1.BDD_table[f2].ID_topVar);  /**< check the top variable is 2 */
   CPPUNIT_ASSERT_EQUAL((ID)3,m1.BDD_table[f2].ID_sink_1);  /**< check if  the high cofactor is 3 */
   CPPUNIT_ASSERT_EQUAL((ID)0,m1.BDD_table[f2].ID_sink_0); /**< check if the low cofactor is 0 */
  }

/// Method for Unit Test: checks or2()
  void OBDD_test::or2Test(void)
  { ///when node is not belong to the table
    CPPUNIT_ASSERT_THROW(m2.or2(25,2),BDDidInvalidException);
    CPPUNIT_ASSERT_THROW(m2.or2(2,25),BDDidInvalidException);
    ///check by TestTable for the operation or(2,3)
    ID f1 = m2.or2(2,3); /**< create function f1 */
    CPPUNIT_ASSERT_EQUAL((ID)6,f1); /**<  check the ID of the function is 6 */
    CPPUNIT_ASSERT_EQUAL((ID)2,m2.BDD_table[f1].ID_topVar);  /**<  check the top variable is 2 */
    CPPUNIT_ASSERT_EQUAL((ID)1,m2.BDD_table[f1].ID_sink_1); /**< check if  the high cofactor is 1 */
    CPPUNIT_ASSERT_EQUAL((ID)3,m2.BDD_table[f1].ID_sink_0); /**<  check if the low cofactor is 3 */

  ///check by creating a table for the function f2
   m1.createVar("a"); /**<create variables */
   m1.createVar("b");
   ID f2 = m1.or2(2,3); /**< create function f */
   CPPUNIT_ASSERT_EQUAL((ID)4,f2); /**< check the ID of the function is 4 */
   CPPUNIT_ASSERT_EQUAL((ID)2,m1.BDD_table[f2].ID_topVar);  /**< check the top variable is 2 */
   CPPUNIT_ASSERT_EQUAL((ID)1,m1.BDD_table[f2].ID_sink_1); /**< check if  the high cofactor is 1 */
   CPPUNIT_ASSERT_EQUAL((ID)3,m1.BDD_table[f2].ID_sink_0); /**< check if the low cofactor is 3 */
  }

  /// Method for Unit Test: checks xor2()
  void OBDD_test::xor2Test(void)
  { ///when node is not belong to the table
   CPPUNIT_ASSERT_THROW(m2.xor2(15,2),BDDidInvalidException);
   CPPUNIT_ASSERT_THROW(m2.xor2(2,15),BDDidInvalidException);
   ///check by creating a table for the function f= xor(a,b)
   m1.createVar("a"); /**<create variables */
   m1.createVar("b");
   ID f = m1.xor2(2,3); /**< create function f */
   CPPUNIT_ASSERT_EQUAL((ID)5,f); /**< check the ID of the function is 5 */
   CPPUNIT_ASSERT_EQUAL((ID)2,m1.BDD_table[f].ID_topVar);  ///**< check if  the high cofactor is 4 */
   CPPUNIT_ASSERT_EQUAL((ID)3,m1.BDD_table[f].ID_sink_0); /**< check if the low cofactor is 3 */
  }

  /// Method for Unit Test: checks neq()
  void OBDD_test::neqTest(void)
  {
    ///when node is not belong to the table
   CPPUNIT_ASSERT_THROW(m2.neq(15),BDDidInvalidException);
   ///check by creating a table for the function f= neq(a.b)
   m1.createVar("a"); /**<create variables */
   m1.createVar("b");
   ID f1 = m1.and2(2,3); /**< create function f1 */
   ID f2 = m1.neq(f1); /**< create function f2 */
   CPPUNIT_ASSERT_EQUAL((ID)6,f2); /**< check the ID of the function is 6 */
   CPPUNIT_ASSERT_EQUAL((ID)2,m1.BDD_table[f2].ID_topVar);  /**< check the top variable is 2 */
   CPPUNIT_ASSERT_EQUAL((ID)5,m1.BDD_table[f2].ID_sink_1); /**< check if  the high cofactor is 5 */
   CPPUNIT_ASSERT_EQUAL((ID)1,m1.BDD_table[f2].ID_sink_0); /**< check if the low cofactor is 1 */
  }

  /// Method for Unit Test: checks nand2()
  void OBDD_test::nand2Test(void)
  { ///when node is not belong to the table
    CPPUNIT_ASSERT_THROW(m2.nand2(15,2),BDDidInvalidException);
    CPPUNIT_ASSERT_THROW(m2.nand2(2,15),BDDidInvalidException);
   ///check by creating a table for the function f= nand(a,b)
   m1.createVar("a"); /**<create variables */
   m1.createVar("b");
   ID f = m1.nand2(2,3); /**< create a function f */
   CPPUNIT_ASSERT_EQUAL((ID)6,f); /**< check the ID of the function is 6 */
   CPPUNIT_ASSERT_EQUAL((ID)2,m1.BDD_table[f].ID_topVar);  /**< check the top variable is 2 */
   CPPUNIT_ASSERT_EQUAL((ID)5,m1.BDD_table[f].ID_sink_1); /**< check if  the high cofactor is 5 */
   CPPUNIT_ASSERT_EQUAL((ID)1,m1.BDD_table[f].ID_sink_0); /**< check if the low cofactor is 1 */
  }

  /// Method for Unit Test: checks nor2()
  void OBDD_test::nor2Test(void)
  { ///when node is not belong to the table
    CPPUNIT_ASSERT_THROW(m2.or2(15,2),BDDidInvalidException);
    CPPUNIT_ASSERT_THROW(m2.or2(2,15),BDDidInvalidException);
    ///check by creating a table for the function f= nor(a,b)
   m1.createVar("a"); /**< create a function f */
   m1.createVar("b"); /**< create variables */
   ID f = m1.nor2(2,3); /**< create a function f */
   CPPUNIT_ASSERT_EQUAL((ID)6,f); /**< check the ID of the function is 6 */
   CPPUNIT_ASSERT_EQUAL((ID)2,m1.BDD_table[f].ID_topVar);  /**< check the top variable is 2 */
   CPPUNIT_ASSERT_EQUAL((ID)0,m1.BDD_table[f].ID_sink_1); /**< check if  the high cofactor is 0 */
   CPPUNIT_ASSERT_EQUAL((ID)5,m1.BDD_table[f].ID_sink_0); /**< check if the low cofactor is 5 */
  }
