/**
 * \class OBDD_test
 *
 * \brief Tests the manager class
 *
 * This class contains the Unit Tests used to test the manager class.
 *
 * \author Viet Tan Nguyen
 * \author Selin Tozman
 *
 */

#pragma once

#ifndef OBDD_TEST_HPP
#define OBDD_TEST_HPP

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "../OBDD.hpp"

using namespace std;
using namespace CppUnit;

//! OBDD_test class.
/*!
  Define the methods to test the manager class
*/

class OBDD_test : public TestFixture
{
  //CppUnit: declare the suite, passing the class name to the macro.
  CPPUNIT_TEST_SUITE(OBDD_test);
  //CppUnit: declare each test case of the fixture in the suite passing the method name.
  CPPUNIT_TEST(constructorTest);
  CPPUNIT_TEST(createVarTest);
  CPPUNIT_TEST(trueTest);
  CPPUNIT_TEST(falseTest);
  CPPUNIT_TEST(isConstantTest);
  CPPUNIT_TEST(isVariableTest);
  CPPUNIT_TEST(topvarTest);
  CPPUNIT_TEST(coFactorTrueTest);
  CPPUNIT_TEST(coFactorFalseTest);
  CPPUNIT_TEST(getTopVarNameTest);
  CPPUNIT_TEST(findVarsTest);
  CPPUNIT_TEST(findNodesTest);
  CPPUNIT_TEST(coFactorTrue2Test);
  CPPUNIT_TEST(coFactorFalse2Test);
  CPPUNIT_TEST(iteTest);
  CPPUNIT_TEST(and2Test);
  CPPUNIT_TEST(or2Test);
  CPPUNIT_TEST(xor2Test);
  CPPUNIT_TEST(neqTest);
  CPPUNIT_TEST(nand2Test);
  CPPUNIT_TEST(nor2Test);

  //CppUnit: end the suite declaration.
  CPPUNIT_TEST_SUITE_END();

public:
  //CppUnit: methods from the base class overridden in this class
  //! Empty constructor of OBDD_test.
    OBDD_test();
   //! Set up context before running a test
  void setUp(void);
  //! Clean up after the test run
  void tearDown(void);
  // Methods with unit tests
  //! Test for constructor of manager class.
  void constructorTest(void);
  //! Test for createVar() of manager class.
  void createVarTest(void);
  //! Test for true() of manager class.
  void trueTest(void);
  //! Test for false() of manager class.
  void falseTest(void);
  //! Test for isConstant() of manager class.
  void isConstantTest(void);
  //! Test for isVariable() of manager class.
  void isVariableTest (void);
  //! Test for topVar() of manager class.
  void topvarTest(void);
  //! Test for coFactorTrue() called by one parameter of manager class.
  void coFactorTrueTest(void);
  //! Test for coFactorFalse() called by one parameter of manager class.
  void coFactorFalseTest(void);
  //! Test for getTopVarName() of manager class.
  void getTopVarNameTest(void);
  //! Test for findVarsTest of manager class.
  void findVarsTest(void);
  //! Test for findNodesTest of manager class.
  void findNodesTest(void);
  //! Test for coFactorTrue() called by two parameters of manager class.
  void coFactorTrue2Test(void);
  //! Test for coFactorFalse() called by two parameters of manager class.
  void coFactorFalse2Test(void);
  //! Test for ite() of manager class.
  void iteTest(void);
  //! Test for and2() of manager class.
  void and2Test(void);
  //! Test for or2() of manager class.
  void or2Test(void);
  //! Test for xor2() of manager class.
  void xor2Test(void);
  //! Test for neq() of manager class.
  void neqTest(void);
  //! Test for nand2() of manager class.
  void nand2Test(void);
  //! Test for nor2() of manager class.
  void nor2Test(void);

private:
  //CppUnit: a fixture is a known set of objects (variables) that serves as a base for a set of test cases.
  //         Note that the same objects will be used in different test methods.
  //         If the same objects are not used in different methods then no fixture is required
  //         and the methods "setUp" and "tearDown" do not need to be overridden

  // CppUnit: add member variables for each part of the fixture

  //! Create two manager objects name as m1 and m2.
  manager m1,m2;
};

#endif
